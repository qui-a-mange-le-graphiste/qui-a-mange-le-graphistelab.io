Ne pas donner à voir! Donner à lire... et écrire.

Une modeste expérimentation sur cette dynamique _lecture <-> écriture_ qui attend que tout graphiste embrasse signes et codes pour penser, écrire, afficher, lire et débattre de sa démarche. Une modeste expérimentation pour un graphisme contemporain.

Une page recto/verso qui n'attend que vous pour évoluer. Un micro-rêve d'édition en bleu, vert ou rouge. Une revue(?) qui n'aura jamais de rythme de parution autre que celui choisi par ses lecteurs/acteurs/auteurs.

Pourquoi une édition en HTML/CSS? Parce qu'un clic-droit sur la version en ligne de cette page vous conduit vers un autre texte - augmenté, commenté... — toujours lisible. Le refus des boîtes noires et/ou des formats propriétaires est une posture poétique/politique — salutation au soleil! Le même travail sur un logiciel de mise en page n'aurait été qu'entropie. Et pourquoi Git? Parce que la gestion de versions en ligne peut supplanter le concept d'édition et de parution — une mise en mémoire de l'action d'écriture. Sans oublier l'impression — ultime mise en mémoire de cette mise en mémoire de l'action d'écriture.

Les règles (molles — puisque sujettes à toutes modifications) sont les suivantes:

+ Le présent objet doit être lisible en ligne (et sur petit écran en mode portrait) et assurer une version papier à imprimer depuis un navigateur web.
+ Le code source reste visible et lisible à tous, et respecte Git pour la gestion des différentes itérations (le projet est pour le moment hébergé sur GitLab mais dans aucune mesure dépendant de cette plate-forme).
+ l'intégralité de la production de cet objet présent et à venir est un don.
+ Une démarche (pour le moment) sans licence.

— pyc — mars 2018

### Mise à jour en période de confinement

+ Rajout d'une référence et d'un lien vers [Une maison d'édition](https://une-maison-d-edition.gitlab.io/)
